use serde_derive::{Deserialize, Serialize};
use std::io::{stdin, Read};

fn main() {
  let query = prompt_imdb_query();
  get_imdb_suggestions(query);

  /*
  let imdb_id = prompt_imd_id();
  let html = get_imdb_html(imdb_id);
  let document = Document::from(&html[..]);
  println!(
      "{}",
      document
          .find(Attr("itemprop", "name"))
          .next()
          .unwrap()
          .text()
  )*/
}

fn _prompt_imd_id() -> String {
  let mut imdb_id = String::new();
  println!("Enter an IMDb ID");
  stdin().read_line(&mut imdb_id).expect("Did not enter a correct string");

  imdb_id
}

fn prompt_imdb_query() -> String {
  let mut query = String::new();
  println!("Search a movie or a tv show");
  stdin().read_line(&mut query).expect("Did not enter a correct string");

  query
}

#[derive(Serialize, Deserialize)]
struct Suggestion {
  d: Vec<SuggestionData>,
  q: String,
}

#[derive(Serialize, Deserialize)]
struct SuggestionData {
  l: String,
  id: String,
  s: String,
  y: u64,
}

// TODO:
// If field missing, script crash -> find a way to allow optional keys in JSON
// If no result, warn the user and prompt again for a search input
// Regex the input to remove all characters not in [a-z]
fn get_imdb_suggestions(query: String) {
  let query = &query[..].trim();
  let url = [
    "https://v2.sg.media-imdb.com/suggests/",
    &query.chars().next().unwrap().to_string()[..],
    "/",
    query,
    ".json",
  ]
  .concat();

  // println!("{}", url);

  let url = reqwest::Url::parse(&url[..]).unwrap();

  let mut res = match reqwest::blocking::get(url) {
    Ok(response) => response,
    Err(error) => panic!("{}", error),
  };

  assert!(res.status().is_success());

  let mut content = String::new();
  res.read_to_string(&mut content).expect("Unable to read the string");

  let to_remove = ["imdb$", &query[..], "("].concat();
  let to_remove = to_remove.chars().count();
  content.pop();
  let content = &content[to_remove..];

  let suggestion: Suggestion = serde_json::from_str(content).unwrap();

  println!("--------------");

  for (i, result) in suggestion.d.iter().enumerate() {
    println!("{}. {} ({}, {})", i + 1, result.l, result.y, result.s);
  }

  let mut stop = String::new();
  stdin().read_line(&mut stop).expect("Error");
}

fn _get_imdb_html(imdb_id: String) -> String {
  let imdb_id = &imdb_id[..].trim();
  let url = ["http://www.imdb.com/title/", imdb_id].concat();
  // println!("URL: {}", url);
  let url = reqwest::Url::parse(&url[..]).unwrap();

  let mut res = match reqwest::blocking::get(url) {
    Ok(response) => response,
    Err(error) => panic!("{}", error),
  };

  assert!(res.status().is_success());

  let mut content = String::new();
  res.read_to_string(&mut content).expect("Unable to read the string");

  content
}
